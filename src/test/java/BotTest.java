/**
 * Created by candrahermanto on 10/12/16.
 */

import com.candra.bot.MainApp;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BotTest {

    MainApp mainApp;

    @Test
    public void testDoConnect() throws Exception {
        mainApp = new MainApp();
        String result = mainApp.doConnect("getMe");
        assertEquals("{\"ok\":true,\"result\":{\"id\":238023054,\"first_name\":\"kutukomp\",\"username\":\"kutukomp_bot\"}}", result);
    }
}
