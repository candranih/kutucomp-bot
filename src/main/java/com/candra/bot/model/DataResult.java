package com.candra.bot.model;

/**
 * Created by candrahermanto on 10/9/16.
 */
public class DataResult {

    private Integer update_id;
    private DataMessage message;

    public Integer getUpdate_id() {
        return update_id;
    }

    public void setUpdate_id(Integer update_id) {
        this.update_id = update_id;
    }

    public DataMessage getMessage() {
        return message;
    }

    public void setMessage(DataMessage message) {
        this.message = message;
    }
}
