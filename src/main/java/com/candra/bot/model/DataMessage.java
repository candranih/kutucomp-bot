package com.candra.bot.model;


import java.util.List;

/**
 * Created by candrahermanto on 10/9/16.
 */
public class DataMessage {

    private String message_id;
    private MessageFrom from;
    private MessageChat chat;
    private String date;
    private String text;
    private List<MessageEntities> entities;
    private NewCharParticipant new_chat_participant;
    private NewChatMember new_chat_member;
    private LeftChatMember left_chat_member;
    private LeftChatParticipant left_chat_participant;

    public LeftChatMember getLeft_chat_member() {
        return left_chat_member;
    }

    public void setLeft_chat_member(LeftChatMember left_chat_member) {
        this.left_chat_member = left_chat_member;
    }

    public LeftChatParticipant getLeft_chat_participant() {
        return left_chat_participant;
    }

    public void setLeft_chat_participant(LeftChatParticipant left_chat_participant) {
        this.left_chat_participant = left_chat_participant;
    }

    public NewCharParticipant getNew_chat_participant() {
        return new_chat_participant;
    }

    public void setNew_chat_participant(NewCharParticipant new_chat_participant) {
        this.new_chat_participant = new_chat_participant;
    }

    public NewChatMember getNew_chat_member() {
        return new_chat_member;
    }

    public void setNew_chat_member(NewChatMember new_chat_member) {
        this.new_chat_member = new_chat_member;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public MessageFrom getFrom() {
        return from;
    }

    public void setFrom(MessageFrom from) {
        this.from = from;
    }

    public MessageChat getChat() {
        return chat;
    }

    public void setChat(MessageChat chat) {
        this.chat = chat;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<MessageEntities> getEntities() {
        return entities;
    }

    public void setEntities(List<MessageEntities> entities) {
        this.entities = entities;
    }
}
