package com.candra.bot.model;

import java.util.List;

/**
 * Created by candrahermanto on 10/9/16.
 */
public class DataStatus {
    private String ok;
    private List<DataResult> result;


    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public List<DataResult> getResult() {
        return result;
    }

    public void setResult(List<DataResult> result) {
        this.result = result;
    }
}
