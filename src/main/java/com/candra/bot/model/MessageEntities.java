package com.candra.bot.model;

/**
 * Created by candrahermanto on 10/9/16.
 */
public class MessageEntities {

    private String type;
    private Integer offset;
    private Integer length;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }
}
