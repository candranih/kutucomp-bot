package com.candra.bot;

import com.candra.bot.model.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.util.List;

/**
 * Created by candrahermanto on 10/9/16.
 */
public class MainApp {
    String urlApi = "https://api.telegram.org";
    String token = "";

    public static void main(String args[]) throws Exception {
        MainApp kBot = new MainApp();

        System.out.println(kBot.getUpdate());
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        while (true) {
            DataStatus data = mapper.readValue(kBot.getUpdate(), DataStatus.class);

            System.out.println(kBot.getUpdate());
            for (DataResult result : data.getResult()) {
                DataMessage m = result.getMessage();
                if(m.getText() !=null) {
                    if (m.getText().equals("/startArchive")) {
                        kBot.sendMessage("Mulai menyimpan chat", m.getChat().getId());


                    } else if(m.getText().equals("/endArchive")) {
                        kBot.sendMessage("Akhir dari archive", m.getChat().getId());
                    }


                } else {
                    NewChatMember newChatMember = m.getNew_chat_member();
                    LeftChatMember leftChatMember = m.getLeft_chat_member();
                    if(newChatMember !=null) {
                        kBot.sendMessage("Selamat Datang " + newChatMember.getFirst_name() +" "+newChatMember.getLast_name(), m.getChat().getId());
                    } else if (leftChatMember !=null) {
                        kBot.sendMessage("Selamat Tinggal " + leftChatMember.getFirst_name() +" "+leftChatMember.getLast_name(), m.getChat().getId());
                    } else {
                        //do nothing
                    }

                }
                Integer offset = result.getUpdate_id() + 1;
                kBot.updateOfsset(offset);

            }
            Thread.sleep(1000);


        }


    }


    public String doConnect(String method) throws Exception {
        URL url = new URL(urlApi + "/bot" + token + "/" + method);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        return response.toString();

    }


    public String sendMessage(String message, int chatId) throws Exception {

        String getMeUrl = urlApi + "/bot" + token + "/sendMessage";

        URL url = new URL(getMeUrl);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);

        String param = "text=" + message + "&chat_id=" + chatId;
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(param);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();

    }

    public void updateOfsset(int updateId) throws Exception {

        String getMeUrl = urlApi + "/bot" + token + "/getUpdates?offset=" + updateId;

        URL url = new URL(getMeUrl);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("POST");
        con.setDoOutput(true);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        System.out.println("response Update :" + response.toString());


    }

    public String getUpdate() throws Exception {

        String getMeUrl = urlApi + "/bot" + token + "/getUpdates";

        URL url = new URL(getMeUrl);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();


        return response.toString();

    }

    public void writeFile(String chat) {

    }
}
